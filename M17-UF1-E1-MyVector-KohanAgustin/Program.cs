﻿using System;
using System.Numerics;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace M17_UF1_E1_MyVector_KohanAgustin
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Ejercicios Agustin";
            Console.CursorVisible = false;          
            MenuObject menu = new MenuObject(new string[] { "Say Hello", "¿Distancia del vector?", "Cambiar direccion del vector", "Info del vector", "Generar Vectores Random", "Mostrar Vector","Salir" });
            
            RunMenuExercices(menu);
        }

        static void RunMenuExercices(MenuObject menu)
        {
            Program program = new Program();
            MyVector vector = new MyVector(new double[,] { { -5, 7 }, { 4, 6 } });

            do
            {
                //Generamos el menu
                menu.Use(menu);
                //Accedemos a una de las opciones del manu
                Console.Clear();

                switch (menu.posCursor)
                {
                    case 0: //Decir Hello
                        program.SayHello();
                        break;
                    case 1: //Distancia del vector
                        Console.WriteLine(vector.Distance() + " unidades");
                        Console.ReadLine();
                        break;
                    case 2: //Cambiar direccion o de sentido?
                        vector.ChangeDirection();
                        break;
                    case 3: //Info del vector
                        Console.WriteLine(vector.ToString());
                        Console.ReadLine();
                        break;
                    case 4: //Generar vectores random
                        randomVectorsExercicie();
                        break;
                    case 5: //Mostrar un vector dibujado
                        VectorGame vectorGame = new VectorGame();
                        vectorGame.ShowVector(vector);
                        Console.ReadLine(); 
                        break;
                }
            } while (menu.posCursor != menu.options.Length - 1);
        }

        static void randomVectorsExercicie()
        {
            VectorGame vectorGame = new VectorGame();
            MyVector[] rndVectors = vectorGame.RandomVectors(5);
            foreach (var actualVector in rndVectors)
                Console.WriteLine(actualVector);
            Console.ReadLine();

            vectorGame.SortVectorsBy(rndVectors, true);

            Console.WriteLine("Ordenado por distancia");
            foreach (var actualVector in rndVectors)
                Console.WriteLine(actualVector);
            Console.ReadLine();

            vectorGame.SortVectorsBy(rndVectors, false);

            Console.WriteLine("Ordenado por proximidad al origen");
            foreach (var actualVector in rndVectors)
                Console.WriteLine(actualVector);
            Console.ReadLine();
        }
        void SayHello()
        {
            Console.WriteLine("What's your name?");
            Console.WriteLine("Hello " + Console.ReadLine());
            Console.ReadLine();
        }      
    }
}
