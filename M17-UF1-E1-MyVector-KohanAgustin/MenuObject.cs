﻿using System;

namespace M17_UF1_E1_MyVector_KohanAgustin
{
    public class MenuObject
    {
        public int posCursor { get; set; }
        public string[] options { get; set; }
 
        public MenuObject(string[] options, int posCursor)
        {
            this.posCursor = posCursor;
            this.options = options;
        }

        public MenuObject(string[] options)
        {
            this.options = options;
            posCursor = 0;
        }

        bool Show(bool clear)
        {
            if (clear)
                Console.Clear();
            Console.ResetColor();
            foreach (var option in options)
                Console.WriteLine(" " + option);
            return true;
        }

        void UpdateCursor(bool keyUp)
        {
            if (keyUp)
            {
                InData.ClearCurrentConsoleLine();
                Console.Write(" " + options[posCursor+1]);
                Console.SetCursorPosition(0,Console.CursorTop-1);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("> " + options[posCursor]);
            }
            else
            {
                InData.ClearCurrentConsoleLine();
                Console.Write(" " + options[posCursor-1]);
                Console.SetCursorPosition(0,Console.CursorTop+1);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("> " + options[posCursor]);
            }
        }
        
        bool Switch(MenuObject menuObject, int titleHigh)
        {
            //Leer la tecla usada
            Console.SetCursorPosition(0,10);
            var key = Console.ReadKey(true).Key;
            Console.SetCursorPosition(0, posCursor+titleHigh);
            Console.ResetColor();
            
            switch (key)
            {
                case ConsoleKey.UpArrow:
                    if (posCursor == 0)
                        return true;
                    posCursor -= 1;
                    menuObject.UpdateCursor(true);
                    break;
                case ConsoleKey.DownArrow:
                    if (posCursor == options.Length - 1)
                        return true;
                    posCursor += 1;
                    menuObject.UpdateCursor(false);
                    break;
                case ConsoleKey.Enter:
                    return false;
                default:
                    return true;
            }
            return true;
        }

        public bool Use(MenuObject menu, string title)
        {
            Console.ResetColor();
            Console.Clear();

            Console.SetCursorPosition(0,0);
            Console.WriteLine(title);
            int titleHigh = Console.CursorTop;
            menu.Show(false);
            Console.SetCursorPosition(0, posCursor + titleHigh);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("> "+ options[posCursor]);
            Console.ResetColor();
            

            while (menu.Switch(menu, titleHigh))
            { }
            return true;
        }

        public bool Use(MenuObject menu)
        {
            Console.ResetColor();
            Console.Clear();

            Console.SetCursorPosition(0, 0);
            int titleHigh = Console.CursorTop;
            menu.Show(false);
            Console.SetCursorPosition(0, posCursor + titleHigh);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("> " + options[posCursor]);
            Console.ResetColor();


            while (menu.Switch(menu, titleHigh))
            { }
            return true;
        }
    }

    public class Option
    {
        string text;
        
    }
}