﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace M17_UF1_E1_MyVector_KohanAgustin
{
    class VectorGame
    {
        public MyVector[] RandomVectors(int numVectors)
        {
            MyVector[] vectors = new MyVector[numVectors];
            for (int i = 0; i < vectors.Length; i++)
            {
                Random rnd = new Random();
                vectors[i] = new MyVector(new double[,] { { rnd.Next(-100, 101), rnd.Next(-100, 101) }, { rnd.Next(-100, 101), rnd.Next(-100, 101) } });
            }
            return vectors;
        }

        public void SortVectorsBy(MyVector[] vectors, bool sortType)
        {
            if (sortType)
            {
                for (int i = 0; i < vectors.Length; i++)
                {
                    for (int j = 0; j < vectors.Length - 1; j++)
                    {
                        if (vectors[j].Distance() < vectors[j + 1].Distance())
                        {
                            (vectors[j], vectors[j + 1]) =
                                (vectors[j + 1],
                                    vectors[j]);
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < vectors.Length; i++)
                {
                    for (int j = 0; j < vectors.Length - 1; j++)
                    {
                        if (vectors[j].ClosestDistanceToOrigin() < vectors[j + 1].ClosestDistanceToOrigin())
                        {
                            (vectors[j], vectors[j + 1]) =
                                (vectors[j + 1],
                                    vectors[j]);
                        }
                    }
                }
            }
        }

        public void ShowVector(MyVector vector)
        {

            var vectorCords = vector.GetVectorCords();
            int leftestPoint;
            int rightestPoint;

            if (vectorCords[0, 0] < vectorCords[1, 0])
            {
                leftestPoint = Convert.ToInt32(vectorCords[0, 0]);
                rightestPoint = Convert.ToInt32(vectorCords[1, 0]);
            }

            else 
            {
                rightestPoint = Convert.ToInt32(vectorCords[0, 0]);
                leftestPoint = Convert.ToInt32(vectorCords[1, 0]);
            }

            char[,] table = new char[20, 20];
            //0,0 =  table[50,50]

            //Como llenar una matriz directamente?
            FillMatrix(table);
            

            for (int x = leftestPoint; x < rightestPoint; x ++)
            {
                var y = Convert.ToInt32(vector.m() * x + vector.b());
                if (x+1 == rightestPoint)
                    table[10 + x, 10 + y] = 'X';
                else
                    table[10 + x, 10 + y] = '#';
                
            }
            
            PrintMatrix(table);
        }

        void FillMatrix(char[,] table)
        {
            for (int i = 0; i < table.GetLength(0); i++)
            {
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    table[i, j] = ' ';
                    if (j == 10 && i == 10)
                        table[i, j] = '0';
                }
            }
        }

        void PrintMatrix(char[,] table)
        {
            for (int i = table.GetLength(0) - 1; i >= 0; i--)
            {
                for (int j = 0; j < table.GetLength(1); j++)
                {
                    Console.Write(table[j, i]);
                }
                Console.Write("\n");
            }
        }
    }
}
