using System;

namespace M17_UF1_E1_MyVector_KohanAgustin
{
    public class InData
    {
        public static string GetString(string mensaje)
        {
            Console.WriteLine(mensaje);
            return Convert.ToString(Console.ReadLine());
        }
        
        public static string GetUpperString(string mensaje)
        {
            Console.WriteLine(mensaje);
            return Convert.ToString(Console.ReadLine()).ToUpper();
        }
        
        public static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth)); 
            Console.SetCursorPosition(0, currentLineCursor);
        }
    }
}