﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M17_UF1_E1_MyVector_KohanAgustin
{
    class MyVector
    {
        double[,] _vectorCords;
        
        
        public MyVector (double[,] vectorCords)
        {
            _vectorCords = vectorCords;
        }

        public MyVector()
        {
            _vectorCords = new double[2,2] { { 0, 0 }, { 0, 0 } };
        }

        public MyVector(double xStartPoint, double yStartPoint, double xEndPoint, double yEndPoint)
        {
            _vectorCords = new double[2, 2] { { xStartPoint, yStartPoint }, { xEndPoint, yEndPoint } };
        }

        public double[,] GetVectorCords()
        {
            return _vectorCords;
        }

        public void SetVectorCord(double[,] vectorCords)
        {
            _vectorCords = vectorCords;
        }

        public double Distance()
        {
            return Math.Sqrt(Math.Pow(_vectorCords[1,0] - _vectorCords[0,0],2)+ Math.Pow(_vectorCords[1,1] - _vectorCords[0,1], 2));
        }


        public void ChangeDirection()
        {
            //(a, b) = (b, a);
            (_vectorCords[0,0], _vectorCords[1,0], _vectorCords[0, 1], _vectorCords[1, 1]) = (_vectorCords[1, 0], _vectorCords[0, 0], _vectorCords[1, 1], _vectorCords[0, 1]);
        }

        public double ClosestDistanceToOrigin()
        {
            var nominator = Math.Abs((_vectorCords[1,0] - _vectorCords[0,0]) * _vectorCords[0,1] -
                                     _vectorCords[0,0] * (_vectorCords[1,1] - _vectorCords[0,1]));
            return nominator / Distance();
        }

        public override string ToString()
        {
            return "StartPoint:["+ _vectorCords[0, 0] + ","+ _vectorCords[0, 1] + "], EndPoint:[" + _vectorCords[1, 0] + "," + _vectorCords[1, 1] + "], Distancia: " + Distance();
        }

        public double m()
        {
            return (_vectorCords[1,1] - _vectorCords[0,1]) /  (_vectorCords[1, 0] - _vectorCords[0, 0]); //(y2-y1) / (x2-x1)
        }

        public double b()
        {
            // y = mx + b
            return _vectorCords[0, 1] - m() * _vectorCords[0,0];
        }

    }
}